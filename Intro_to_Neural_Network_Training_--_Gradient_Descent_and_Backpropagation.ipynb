{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "7f03114e",
   "metadata": {},
   "source": [
    "# Table of Contents\n",
    "1. Inputs: Features and Targets\n",
    "    1. Feature Extraction and Preparation\n",
    "2. Neural network units (TLUs)\n",
    "    1. Perceptrons\n",
    "    2. Neurons with Other Activation Functions\n",
    "    3. Weighting Inputs\n",
    "3. Outputs and Error: Now We're Learning\n",
    "    1. Cost Functions\n",
    "    2. Gradients and Differentiability\n",
    "    3. Backpropagation\n",
    "    4. Update Rules: the Delta Rule\n",
    "4. Computational Efficiency\n",
    "    1. Autodiff\n",
    "    2. Stochastic Gradient Descent\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1414e2a2",
   "metadata": {},
   "source": [
    "# Input: Features and Targets <a name=\"InputsFeaturesandTargets\"></a>\n",
    "In the most usual case, machine learning models are software that learns patterns from data, and uses those patterns to make predictions about new data. Those predictions might be classification problems, where the model determines which of 2 or more categories an item of interest belongs to (e.g. does a photo of a bump on someone's skin contain a cancer? If so, what kind?) or regression problems, over continuous variables (e.g. What will a share in a mutual fund be worth 6 months from now?)\n",
    "\n",
    "With some notable exceptions (such as generative adversarial networks), deep neural nets are trained using *supervised learning*, which means they are provided input data **X** containing *feature* vectors for each item that is to be learned from, and *target* data **Y** containing the desiderata that are to be inferred about each item. In other words, supervised learning means that at some point, correct answers are provided, and an algorithm uses those targets to parameterize the computations that the model performs to make its predictions.\n",
    "\n",
    "Raw data may be photographs, texts, numerical data, or even digital representations of acoustic data. One way or another, the raw data must be engineered until it is a matrix of numerical values, which the computing units of a neural network are able to use. (The details of feature engineering are outside of the scope of this talk, but it's a rich topic in and of itself.)\n",
    "\n",
    "# Neural network units (TLUs)\n",
    "A neural network is an inference network made of layers of nodes, each of which accepts one of more inputs, applies a weighting function to them, and then applies an activation function to that weighted sum. There's more than one kind of neural network unit, aka *threshold logic unit* or TLU.\n",
    "\n",
    "## Perceptrons\n",
    "The earliest implementation of a TLU was the Perceptron, a logic unit that uses a step function as its activation function. In other words, its activation values are strictly binary, 0 or 1, with a discrete step between them. Perceptrons are great for modeling many kinds of discrete logic, but they suffer from a severe limitation: their activation is not smooth. It's derivative is zero everywhere except for where it turns on, and at that step, it has no definable derivative. That means a lot of information is lost about *how much* some input activates a neuron, or more interestingly, how much one neuron contribites to activating another to which it provides inputs to.\n",
    "\n",
    "Another limitation of perceptrons is that there is no way to model the XOR logic gate with a single layer of them. However, multiple layers of perceptrons *can* model XOR logic.\n",
    "\n",
    "There are ways of getting past these limitations of simple perceptron networks. These birthed *deep neural networks,* which can model complex objects and datasets.\n",
    "\n",
    "\n",
    "Here's what a stock \"Perceptron\" object from Python's scikit-learn library looks like under the hood:\n",
    "https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Perceptron.html\n",
    "\n",
    "And here it is as code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "fbe316df",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import Perceptron\n",
    "\n",
    "demo_perceptron = Perceptron()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b2825697",
   "metadata": {},
   "source": [
    "## Neurons with Other Activation Functions\n",
    "Step functions aren't the only activation functions. Others that are commonly used are tanh, nd sigmoids such as the logistic function\n",
    "\n",
    "Unlike Perceptrons, some neurons that have smooth, everywhere-differentiable activation functions. It's this differentiablity that makes the iterative, computational training of deep neural networks possible.\n",
    "\n",
    "Here are some example activation functions, and a comparison of their applications and tradeoffs:\n",
    "https://en.wikipedia.org/wiki/Activation_function#Comparison_of_activation_functions\n",
    "\n",
    "In this demo, we'll use tanh as our activation function, because of properties of its derivative and its tidy behavior at the origin."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dc751cea",
   "metadata": {},
   "source": [
    "## Feature Extraction and Preparation\n",
    "We will look at the features and targets for one relatively-easy-to-grasp dataset: the MNIST handwriting dataset, comprising 8x8-pixel greyscale images of handwritten digits as the features (a 64-dimensional feature space comprising the color values of the pixels in each position in the images), and the digits they represnt as targets (a 1-dimensional target space).\n",
    "\n",
    "Here are those images: https://en.wikipedia.org/wiki/MNIST_database#/media/File:MnistExamples.png\n",
    "\n",
    "And here's what that looks like in code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "3d2f9877",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      ".. _digits_dataset:\n",
      "\n",
      "Optical recognition of handwritten digits dataset\n",
      "--------------------------------------------------\n",
      "\n",
      "**Data Set Characteristics:**\n",
      "\n",
      "    :Number of Instances: 1797\n",
      "    :Number of Attributes: 64\n",
      "    :Attribute Information: 8x8 image of integer pixels in the range 0..16.\n",
      "    :Missing Attribute Values: None\n",
      "    :Creator: E. Alpaydin (alpaydin '@' boun.edu.tr)\n",
      "    :Date: July; 1998\n",
      "\n",
      "This is a copy of the test set of the UCI ML hand-written digits datasets\n",
      "https://archive.ics.uci.edu/ml/datasets/Optical+Recognition+of+Handwritten+Digits\n",
      "\n",
      "The data set contains images of hand-written digits: 10 classes where\n",
      "each class refers to a digit.\n",
      "\n",
      "Preprocessing programs made available by NIST were used to extract\n",
      "normalized bitmaps of handwritten digits from a preprinted form. From a\n",
      "total of 43 people, 30 contributed to the training set and different 13\n",
      "to the test set. 32x32 bitmaps are divided into nonoverlapping blocks of\n",
      "4x4 and the number of on pixels are counted in each block. This generates\n",
      "an input matrix of 8x8 where each element is an integer in the range\n",
      "0..16. This reduces dimensionality and gives invariance to small\n",
      "distortions.\n",
      "\n",
      "For info on NIST preprocessing routines, see M. D. Garris, J. L. Blue, G.\n",
      "T. Candela, D. L. Dimmick, J. Geist, P. J. Grother, S. A. Janet, and C.\n",
      "L. Wilson, NIST Form-Based Handprint Recognition System, NISTIR 5469,\n",
      "1994.\n",
      "\n",
      ".. topic:: References\n",
      "\n",
      "  - C. Kaynak (1995) Methods of Combining Multiple Classifiers and Their\n",
      "    Applications to Handwritten Digit Recognition, MSc Thesis, Institute of\n",
      "    Graduate Studies in Science and Engineering, Bogazici University.\n",
      "  - E. Alpaydin, C. Kaynak (1998) Cascading Classifiers, Kybernetika.\n",
      "  - Ken Tang and Ponnuthurai N. Suganthan and Xi Yao and A. Kai Qin.\n",
      "    Linear dimensionalityreduction using relevance weighted LDA. School of\n",
      "    Electrical and Electronic Engineering Nanyang Technological University.\n",
      "    2005.\n",
      "  - Claudio Gentile. A New Approximate Maximal Margin Classification\n",
      "    Algorithm. NIPS. 2000.\n",
      "\n"
     ]
    }
   ],
   "source": [
    "from sklearn.datasets import load_digits\n",
    "\n",
    "digit_data = load_digits()\n",
    "print(digit_data.DESCR)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "5ee2d76f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['pixel_0_0', 'pixel_0_1', 'pixel_0_2', 'pixel_0_3', 'pixel_0_4', 'pixel_0_5', 'pixel_0_6', 'pixel_0_7', 'pixel_1_0', 'pixel_1_1', 'pixel_1_2', 'pixel_1_3', 'pixel_1_4', 'pixel_1_5', 'pixel_1_6', 'pixel_1_7', 'pixel_2_0', 'pixel_2_1', 'pixel_2_2', 'pixel_2_3', 'pixel_2_4', 'pixel_2_5', 'pixel_2_6', 'pixel_2_7', 'pixel_3_0', 'pixel_3_1', 'pixel_3_2', 'pixel_3_3', 'pixel_3_4', 'pixel_3_5', 'pixel_3_6', 'pixel_3_7', 'pixel_4_0', 'pixel_4_1', 'pixel_4_2', 'pixel_4_3', 'pixel_4_4', 'pixel_4_5', 'pixel_4_6', 'pixel_4_7', 'pixel_5_0', 'pixel_5_1', 'pixel_5_2', 'pixel_5_3', 'pixel_5_4', 'pixel_5_5', 'pixel_5_6', 'pixel_5_7', 'pixel_6_0', 'pixel_6_1', 'pixel_6_2', 'pixel_6_3', 'pixel_6_4', 'pixel_6_5', 'pixel_6_6', 'pixel_6_7', 'pixel_7_0', 'pixel_7_1', 'pixel_7_2', 'pixel_7_3', 'pixel_7_4', 'pixel_7_5', 'pixel_7_6', 'pixel_7_7'] \n",
      "\n",
      "[0 1 2 3 4 5 6 7 8 9] \n",
      "\n",
      "[ 0.  0.  0. 12. 13.  5.  0.  0.  0.  0.  0. 11. 16.  9.  0.  0.  0.  0.\n",
      "  3. 15. 16.  6.  0.  0.  0.  7. 15. 16. 16.  2.  0.  0.  0.  0.  1. 16.\n",
      " 16.  3.  0.  0.  0.  0.  1. 16. 16.  6.  0.  0.  0.  0.  1. 16. 16.  6.\n",
      "  0.  0.  0.  0.  0. 11. 16. 10.  0.  0.] \n",
      "\n",
      "64\n"
     ]
    }
   ],
   "source": [
    "print(digit_data.feature_names, \"\\n\")\n",
    "print(digit_data.target_names, \"\\n\")\n",
    "print(digit_data.data[1], \"\\n\")\n",
    "print(digit_data.data[1].size)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60f13b1c",
   "metadata": {},
   "source": [
    "So the input layer of a NN that trains off of this data needs to have 64 nodes, while its output layer -- the one that returns the result of the evaluation -- needs to have just one, which yields a digit 0-9."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14df2c0b",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "eda2c887",
   "metadata": {},
   "source": [
    "\n",
    "# Outputs and Error: Now We're Learning\n",
    "## Initialization\n",
    "\n",
    "## Cost Functions (Loss Functions)\n",
    "So when a neural network has is presented with some data and a target, how is it supposed to learn? What does that mean?\n",
    "\n",
    "First: it's important that the initial weights are randomized. When the network is naive, before it has seen any data, it's best if there is no pattern correlating the weights with one another. The weighting needs to be learned *from the data*. If there's a linear relationship between the weights applied by the nodes (e.g. if they're all set to the same value, or multiples of the same value), the network will be effectively collapsed into a single node.  For the same reason, the relationship between the weights in successive layers (in multi-layer networks) must also be nonlinearly related; if they are linearly related, they might as well not be separate layers. It's the differences between nodes that make learning possible: without nonlinear relationships in the network, too much of its behavior would be locked-in (internally dependent) and impossible to update via training.\n",
    "\n",
    "\n",
    "Secondly, we need to tell our network that there's a cost associated with getting an estimate wrong. This is called a *cost function* or *loss function*. These provide a quantitative value for how much some error is to be avoided. The goal of trainig is to set the weights of the network to minimize the output of the cost function.\n",
    "\n",
    "Some examples of cost functions can be found here:\n",
    "https://stats.stackexchange.com/questions/154879/a-list-of-cost-functions-used-in-neural-networks-alongside-applications#:~:text=Here%20are%20those%20I%20understand%20so%20far.\n",
    "\n",
    "\n",
    "\n",
    "## Backpropagation\n",
    "Via Wikipedia:\n",
    ">The backpropagation algorithm works by computing the gradient of the loss function with respect to each weight by the chain rule, computing the gradient one layer at a time, iterating backward from the last layer to avoid redundant calculations of intermediate terms in the chain rule; this is an example of dynamic programming.[3]\n",
    "\n",
    ">The term backpropagation strictly refers only to the algorithm for computing the gradient, not how the gradient is used; however, the term is often used loosely to refer to the entire learning algorithm, including how the gradient is used, such as by stochastic gradient descent.[4] Backpropagation generalizes the gradient computation in the delta rule, which is the single-layer version of backpropagation, and is in turn generalized by automatic differentiation, where backpropagation is a special case of reverse accumulation (or \"reverse mode\").\n",
    "\n",
    "In other words, it's any algorithm by which a network adjusts its reasoning by learning from its mistakes: presenting its errors to its own earlier assumptions and adjusting its subsequent computations accordingly.\n",
    "\n",
    "Isn't that lovely? I wish everyone could learn from this example: comparing their predictions and estimates to measurable reality and then making corrections until something converged. [We could all be so accurate, eventually! There might even be congruence between our models!](https://www.youtube.com/watch?v=0pvMCu_YeYU)\n",
    "\n",
    "\n",
    "## Update Rules: The Delta Rule\n",
    "In the case of a single-layer network (one with an input later and only one other layer), the weights are learned via the following update rule. \n",
    "\n",
    "$$ \\Delta w_{ji}=\\alpha(t_j-y_j) g'(h_j) x_i $$\n",
    "\n",
    "You can find the full derivation of this formula on Wikipedia or in chapter 5 of Chris Bishop's excellent text *Pattern Recognition and Machine Learning.*\n",
    "\n",
    "Here's what everything we see here means:\n",
    "\n",
    "\n",
    "\n",
    "$j$  is a given neuron\n",
    "\n",
    "$i$  is an input received by that neuron -- in the case of a single-layer network, this comes form the data itself. (In multilayer networks, the inputs may come data or from earlier layers of neurons.)\n",
    "\n",
    "$w_ji$  is the weight applied by j, to an input. \n",
    "\n",
    "\n",
    "$y_j$  is the output yielded by the neuron\n",
    "\n",
    "$t_j$  is the target: the data telling us the canonical correct answer for a training datum\n",
    "\n",
    "$x_i$  is the feature data\n",
    "\n",
    "$h_j$ is the sum over i of all these weighted inputs, which is fed to the neuron's activation function.)\n",
    "\n",
    "$g$  is the activation function (tanh, the logistic function, etc). \n",
    "\n",
    "$g'$ is the derivative of $g$, which is why having a easily-differentiable activation function is important. The partial derivative of the output of the activation function, $y_j$ with respect to each input $x_i$ determines the update that's given to the weight applied to each input.\n",
    "\n",
    "\n",
    "$\\alpha$  is the learning rate of the network. It's a proportionality constant that determines how much of the actual difference between the target and the output is used to update the weights. Usually alpha is small, like 0.001, or even smaller. The bigger it is, the bigger of an updage will be applied at each iteration of training (called an *epoch*). The smaller it is, the more training epochs you'll need to update the weights until the utputs converge with the target. You *can* adjust $\\alpha$ to be a function rather than a constant (e.g. you might let it be bigger in the beginning so the network learns greedily, then smaller after each epoch so that it fine-tunes without overshooting), or to be different for some layers than others. But basically, it's a proportionality constant, and it's a lot smaller than 1.\n",
    "\n",
    "\n",
    "**The easy differentiability of the neurons activations functions with respect to the cost function makes automated learning tractable.**\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "## Computational Efficiency\n",
    "Deep neural networks can be powerful, capable of modeling highly nonlinear systems with no human intervention besides setting the training paramaters. But the tradeoff is that training them is much more computationally more demanding than simple, linear, binary Perceptrons.\n",
    "\n",
    "\n",
    "## Autodiff\n",
    "Autodiff is a computational method used to quickly find the partial derivatives for the weights over the many, many nodes in the layers in a multilayer network. One autodiff algorithm is implemented simply by taking the Jacobian matrix, where each entry is simply the partial derivative of each output wrt each input:\n",
    "$$ J_{ki} = \\frac{\\partial y_k}{\\partial x_i} $$\n",
    "\n",
    "\n",
    "To minimize an error function $E$ wrt a parameter $w$, the zeroes of the derivative of the error function can be found using the following expression, incorporating the Jacobian\n",
    "\n",
    "$$ \\frac{\\partial E}{\\partial w} = \\sum_{k,j} \\frac{\\partial E}{\\partial y_k} \\frac{\\partial y_k}{\\partial z_j} \\frac{\\partial z_j}{\\partial w}$$\n",
    "\n",
    "where the derivatives wrt first-layer weights are represented by $\\partial w$ and those wrt second-layer weights are represented by $\\partial z$.\n",
    "\n",
    "*Note: if there is an error in this presentation, it's in the previous line. See Bishop 5.4 and 5.4 for the most solid presentation of the exact algorithm for learnign error in the forward direction, then reverse-accumulating that error to correct the weights backwards.*\n",
    "\n",
    "## Stochastic Gradient Descent\n",
    "Stochastic Gradient Descent takes advantage of the continuous nature of a good loss function, and spares the algorithm from having to calculate the exact update for every node in a network. It's based on the presumption that a sample of the gradient at any node will be close to the gradient for nearby nodes. Thus, at each trainig epoch, a subset of nodes in the network are selected to have their loss gradients computed, and that figure is used as an estimate for updating other neurons' weights. Over the course of many training epochs, a \"fair\" amount of the network is sampled for its opinion about the gradient, and this is distributed into the final result, allowing for a faster, less computationally-intense training process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "62d690cc",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "MLPClassifier(activation='tanh', alpha=1e-05, early_stopping=True, solver='sgd')"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import numpy as np\n",
    "from sklearn.neural_network import MLPClassifier\n",
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "\n",
    "X, y = load_digits(return_X_y=True)\n",
    "X_train, X_test, y_train, y_test = train_test_split(X,y, train_size=0.25, random_state=4669)\n",
    "\n",
    "clf_1 = MLPClassifier(solver='sgd', activation='tanh', alpha=1e-4, early_stopping=True)\n",
    "clf_2 = MLPClassifier(solver='sgd', activation='tanh', alpha=1e-5, early_stopping=True)\n",
    "# from the docs:\n",
    "# \"This model optimizes the log-loss function using LBFGS or stochastic gradient descent.\"\"\n",
    "\n",
    "\n",
    "clf_1.fit(X_train, y_train)\n",
    "clf_2.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "c5b10eb0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([6, 9, 1, ..., 4, 2, 9])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "clf_1.predict(X_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "e1c3dd5a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([6, 8, 1, ..., 4, 2, 3])"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "clf_2.predict(X_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b96c8c7",
   "metadata": {},
   "source": [
    "Even this super-simple example shows how the adjustment of the learning rate alone can result in different classifications by the final product."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f309b585",
   "metadata": {},
   "source": [
    "# TensorFlow example\n",
    "What follows is a demo showing the implementation of a similar classifier using TensorFlow. This one uses multiple layers, rather than the single-layer of the previous example.\n",
    "\n",
    "My goal here was to produce some graphs showing you how the error rate converges as the training epochs proceed. However, I'm not actually super familiar with the TensorFlow and Keras APIs, and was unable to complete this demo before our meeting.\n",
    "\n",
    "Someone else's very nice example can be found here:\n",
    "https://machinelearningmastery.com/display-deep-learning-model-training-history-in-keras/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "c73946bc",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(449, 64)"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import tensorflow as tf\n",
    "from tensorflow import keras\n",
    "\n",
    "X, y = load_digits(return_X_y=True)\n",
    "X_train, X_test, y_train, y_test = train_test_split(X,y, train_size=0.25, random_state=4669) #same as before\n",
    "X_train.data.shape\n",
    "#y_train.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "405764c3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<bound method Network.summary of <tensorflow.python.keras.engine.sequential.Sequential object at 0x7fd218158f90>>"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "model = keras.models.Sequential()\n",
    "model.add(keras.layers.Flatten(input_shape=[8,8]))\n",
    "model.add(keras.layers.Dense(64, activation='tanh'))\n",
    "model.add(keras.layers.Dense(32, activation='tanh'))\n",
    "model.add(keras.layers.Dense(10, activation='softmax')) # this is our output layer\n",
    "model.summary"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "ce61b0b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "model.compile(loss='sparse_categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "b96149c1",
   "metadata": {},
   "outputs": [
    {
     "ename": "ValueError",
     "evalue": "Error when checking input: expected flatten_3_input to have 3 dimensions, but got array with shape (449, 64)",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mValueError\u001b[0m                                Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-19-463d8abb866e>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mhistory\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mmodel\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mfit\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mX_train\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0my_train\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mepochs\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;36m30\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mvalidation_split\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;36m0.1\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m~/anaconda3/envs/GradientDescent/lib/python3.7/site-packages/tensorflow_core/python/keras/engine/training.py\u001b[0m in \u001b[0;36mfit\u001b[0;34m(self, x, y, batch_size, epochs, verbose, callbacks, validation_split, validation_data, shuffle, class_weight, sample_weight, initial_epoch, steps_per_epoch, validation_steps, validation_freq, max_queue_size, workers, use_multiprocessing, **kwargs)\u001b[0m\n\u001b[1;32m    726\u001b[0m         \u001b[0mmax_queue_size\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mmax_queue_size\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    727\u001b[0m         \u001b[0mworkers\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mworkers\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 728\u001b[0;31m         use_multiprocessing=use_multiprocessing)\n\u001b[0m\u001b[1;32m    729\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    730\u001b[0m   def evaluate(self,\n",
      "\u001b[0;32m~/anaconda3/envs/GradientDescent/lib/python3.7/site-packages/tensorflow_core/python/keras/engine/training_v2.py\u001b[0m in \u001b[0;36mfit\u001b[0;34m(self, model, x, y, batch_size, epochs, verbose, callbacks, validation_split, validation_data, shuffle, class_weight, sample_weight, initial_epoch, steps_per_epoch, validation_steps, validation_freq, **kwargs)\u001b[0m\n\u001b[1;32m    222\u001b[0m           \u001b[0mvalidation_data\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mvalidation_data\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    223\u001b[0m           \u001b[0mvalidation_steps\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mvalidation_steps\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 224\u001b[0;31m           distribution_strategy=strategy)\n\u001b[0m\u001b[1;32m    225\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    226\u001b[0m       \u001b[0mtotal_samples\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0m_get_total_number_of_samples\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mtraining_data_adapter\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m~/anaconda3/envs/GradientDescent/lib/python3.7/site-packages/tensorflow_core/python/keras/engine/training_v2.py\u001b[0m in \u001b[0;36m_process_training_inputs\u001b[0;34m(model, x, y, batch_size, epochs, sample_weights, class_weights, steps_per_epoch, validation_split, validation_data, validation_steps, shuffle, distribution_strategy, max_queue_size, workers, use_multiprocessing)\u001b[0m\n\u001b[1;32m    514\u001b[0m         \u001b[0mbatch_size\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mbatch_size\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    515\u001b[0m         \u001b[0mcheck_steps\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;32mFalse\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 516\u001b[0;31m         steps=steps_per_epoch)\n\u001b[0m\u001b[1;32m    517\u001b[0m     (x, y, sample_weights,\n\u001b[1;32m    518\u001b[0m      \u001b[0mval_x\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mval_y\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m~/anaconda3/envs/GradientDescent/lib/python3.7/site-packages/tensorflow_core/python/keras/engine/training.py\u001b[0m in \u001b[0;36m_standardize_user_data\u001b[0;34m(self, x, y, sample_weight, class_weight, batch_size, check_steps, steps_name, steps, validation_split, shuffle, extract_tensors_from_dataset)\u001b[0m\n\u001b[1;32m   2470\u001b[0m           \u001b[0mfeed_input_shapes\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   2471\u001b[0m           \u001b[0mcheck_batch_axis\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;32mFalse\u001b[0m\u001b[0;34m,\u001b[0m  \u001b[0;31m# Don't enforce the batch size.\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m-> 2472\u001b[0;31m           exception_prefix='input')\n\u001b[0m\u001b[1;32m   2473\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   2474\u001b[0m     \u001b[0;31m# Get typespecs for the input data and sanitize it if necessary.\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m~/anaconda3/envs/GradientDescent/lib/python3.7/site-packages/tensorflow_core/python/keras/engine/training_utils.py\u001b[0m in \u001b[0;36mstandardize_input_data\u001b[0;34m(data, names, shapes, check_batch_axis, exception_prefix)\u001b[0m\n\u001b[1;32m    563\u001b[0m                            \u001b[0;34m': expected '\u001b[0m \u001b[0;34m+\u001b[0m \u001b[0mnames\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0mi\u001b[0m\u001b[0;34m]\u001b[0m \u001b[0;34m+\u001b[0m \u001b[0;34m' to have '\u001b[0m \u001b[0;34m+\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    564\u001b[0m                            \u001b[0mstr\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mlen\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mshape\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;34m+\u001b[0m \u001b[0;34m' dimensions, but got array '\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 565\u001b[0;31m                            'with shape ' + str(data_shape))\n\u001b[0m\u001b[1;32m    566\u001b[0m         \u001b[0;32mif\u001b[0m \u001b[0;32mnot\u001b[0m \u001b[0mcheck_batch_axis\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    567\u001b[0m           \u001b[0mdata_shape\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mdata_shape\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;36m1\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mValueError\u001b[0m: Error when checking input: expected flatten_3_input to have 3 dimensions, but got array with shape (449, 64)"
     ]
    }
   ],
   "source": [
    "history = model.fit(X_train, y_train, epochs=30, validation_split=0.1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5d5d51fb",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
